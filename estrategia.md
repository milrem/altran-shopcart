Para a solução de carrinho de compras foi utilizada uma arquitetura de aplicação java web simples, com toda a estrutura funcional no backend utilizando RestFull.

Para o desenvolvimento, optei por iniciar pela definição básica dos POJOs de dados Item, User e Cart, utilizando a library Lombok para simplificar a construção das mesmas.
Com os POJOs de dados definidos adotei o TDD como metodologia básica, com a contrução dos testes unitários das regras definidas para o backend, de forma a garantir a qualidade do código desde o início do processo de desenvolvimento.

Infelismente o momento de execução do teste não foi um bom momento para mim, pois estou com um grande acúmulo de tarefas e trabalhos em andamento o que não permitiu que eu pudesse dedicar tanto tempo quanto eu gostaria ao projeto, isso aliado ao fato de eunão ter esperiência prática com Angular, que foi uma das escolhar tecnológicas exigidas como parte do teste, fez com que a minha conclusão seja bem inferior ao desejavel.

Faço a entrega do projeto consciente que poderia ter feito muito melhor, e fico na espectativa de que possa surgir uma outra oportunidade para repetir o processo no futuro, visto que dificilmente o resultado apresentado será suficientemente bom.
