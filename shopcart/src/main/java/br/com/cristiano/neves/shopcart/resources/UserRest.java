package br.com.cristiano.neves.shopcart.resources;

import br.com.cristiano.neves.shopcart.model.User;
import br.com.cristiano.neves.shopcart.service.UserService;
import java.net.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author 
 */
@RestController
@RequestMapping(path = "user", produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
public class UserRest extends RestService<User, String> {
    private static final long LIMIT_ON_lIST = 100;

    @Autowired
    public UserRest(UserService service) {
        super(service);
    }
    
    @Override
    protected String getId(User entity) {
        return entity.getId();
    }
    
    @PostMapping(path = "{name}/{email}")
    public ResponseEntity<User> insert(@PathVariable("name") String name, @PathVariable("email") String email) {
        User created = ((UserService) this.service).create(name, email);
        if (created == null) {
            return ResponseEntity.unprocessableEntity().build();
        }
        URI location = ServletUriComponentsBuilder.fromCurrentServletMapping().path("user/{id}").buildAndExpand(getId(created)).toUri();
        return ResponseEntity.created(location).build();
    }
}
