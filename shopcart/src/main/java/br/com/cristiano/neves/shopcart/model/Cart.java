/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cristiano.neves.shopcart.model;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.NonNull;

/**
 *
 * @author Cristiano Neves
 */
public @Data class Cart {
    private String id;
    private String owner;
    @NonNull
    private final List<Item> items = new ArrayList<>();
}
