/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cristiano.neves.shopcart.resources;

import br.com.cristiano.neves.shopcart.service.IService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author milrem
 */
@CrossOrigin(origins = "*")
public abstract class RestService<T, K> {
    
    protected IService<T, K> service;

    public RestService(@Autowired IService<T, K> service) {
        this.service = service;
    }
    
    protected abstract K getId(T entity);
    
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<T> insert(@RequestBody T entity) {
        T created = this.service.create(entity);
        if (created == null) {
            return ResponseEntity.unprocessableEntity().build();
        }
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{id}").buildAndExpand(getId(entity)).toUri();
        return ResponseEntity.created(location).build();
    }

    @GetMapping
    public ResponseEntity<List<T>> list() {
        return ResponseEntity.ok(this.service.list());
    }

    @GetMapping(path = "{id}")
    public ResponseEntity getById(@PathVariable(value = "id") K id) {
        T entity = this.service.find(id);
        if (entity == null) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(entity);
    }

    @PutMapping(path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@PathVariable(value = "id") K id, @RequestBody T entity) {
        T updated = this.service.update(entity);
        if (updated == null) {
            return ResponseEntity.unprocessableEntity().build();
        }
        return ResponseEntity.ok(updated);
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity delete(@PathVariable(value = "id") K id) {
        T entity = this.service.find(id);
        if (entity == null) {
            return ResponseEntity.notFound().build();
        }
        T deleted = this.service.delete(entity);
        if (deleted == null) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(deleted);
    }
    
}
