/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cristiano.neves.shopcart.service;

import java.util.List;

/**
 *
 * @author Cristiano Neves
 * @param <T> Type of entity that service undertands
 * @param <K> Type of the identity field used on the entity type
 */
public interface IService<T,K> {

    T create(T entity);

    T delete(T entity);

    T find(K id);

    List<T> list();

    T update(T entity);
    
}
