/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cristiano.neves.shopcart.service;

import br.com.cristiano.neves.shopcart.data.UserRepository;
import br.com.cristiano.neves.shopcart.model.User;
import java.util.List;
import java.util.UUID;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

/**
 *
 * @author milrem
 */
@Service
public class UserService implements IService<User, String> {
    private UserRepository repository;
    
    @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
    }
    
    private String makeNewId() {
        String id = UUID.randomUUID().toString();
        if (repository.existsById(id)) {
            return makeNewId();
        } else {
            return id;
        }
    }
    
    @Override
    public User create(User entity) {
        User newUser = new User();
        newUser.setId(makeNewId());
        newUser.setName(entity.getName());
        newUser.setEmail(entity.getEmail());
        repository.insert(newUser);
        return repository.findById(newUser.getId()).get();
    }
    
    @SneakyThrows
    public User create(@NonNull String name, @NonNull String email) {
        User newUser = new User();
        newUser.setEmail(email);
        if (repository.findOne(Example.of(newUser)).isPresent())
            throw new Exception("An User with that email exists");
        
        newUser.setId(makeNewId());
        newUser.setName(name);
        return create(newUser);
    }
    
    @Override
    public List<User> list() {
        return repository.findAll();
    }
    
    @Override
    public User find(String id) {
        return repository.findById(id).get();
    }
    
    @Override
    public User update(User user) {
        User find = new User();
        find.setId(user.getId());
        if (!repository.exists(Example.of(find)))
            return null;
        return repository.save(user);
    }
    
    @Override
    public User delete(User user) {
        User find = new User();
        find.setId(user.getId());
        if (!repository.exists(Example.of(find)))
            return null;
        repository.delete(user);
        return find;
    }
}
