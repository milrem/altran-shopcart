/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cristiano.neves.shopcart.model;

import lombok.Data;

/**
 *
 * @author Cristiano Neves
 */
public @Data  class Item {
    private String id;
    private String name;
    private Float price;
}
