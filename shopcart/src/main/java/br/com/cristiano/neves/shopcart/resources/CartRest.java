package br.com.cristiano.neves.shopcart.resources;

import br.com.cristiano.neves.shopcart.model.Cart;
import br.com.cristiano.neves.shopcart.service.CartService;
import java.net.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author Cristiano Neves
 */
@RestController
@RequestMapping(path = "cart", produces = MediaType.APPLICATION_JSON_VALUE)
public class CartRest extends RestService<Cart, String> {

    @Autowired
    public CartRest(CartService service) {
        super(service);
    }

    @Override
    protected String getId(Cart entity) {
        return entity.getId();
    }

    @PostMapping(path = "{ownerId}")
    public ResponseEntity<Cart> insert(@PathVariable("ownerId") String ownerId) {
        Cart created = ((CartService) this.service).create(ownerId);
        if (created == null) {
            return ResponseEntity.unprocessableEntity().build();
        }
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{id}").buildAndExpand(created.getId()).toUri();
        return ResponseEntity.created(location).build();
    }
    
}
