/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cristiano.neves.shopcart.service;

import br.com.cristiano.neves.shopcart.data.ItemRepository;
import br.com.cristiano.neves.shopcart.model.Item;
import java.util.List;
import java.util.UUID;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

/**
 *
 * @author milrem
 */
@Service
public class ItemService implements IService<Item,String> {
    private ItemRepository repository;
    
    @Autowired
    public ItemService(ItemRepository repository) {
        this.repository = repository;
    }
    
    private String makeNewId() {
        String id = UUID.randomUUID().toString();
        if (repository.existsById(id)) {
            return makeNewId();
        } else {
            return id;
        }
    }
    
    @SneakyThrows
    @Override
    public Item create(Item entity) {
        if (entity.getPrice().compareTo(0f) <= 0)
            throw new Exception("An Item can't have a zero or lower price");
        
        Item newItem = new Item();
        newItem.setName(entity.getName());
        if (repository.findOne(Example.of(newItem)).isPresent())
            throw new Exception("An Item with that name exists, try updating that item or choose another name");
        
        newItem.setId(makeNewId());
        newItem.setPrice(entity.getPrice());
        repository.insert(newItem);
        return repository.findById(newItem.getId()).get();
    }
    
    @Override
    public List<Item> list() {
        return repository.findAll();
    }
    
    @Override
    public Item find(String id) {
        return repository.findById(id).get();
    }
    
    @Override
    public Item update(Item entity) {
        if (!repository.existsById(entity.getId()))
            return null;
        return repository.save(entity);
    }
    
    @Override
    public Item delete(Item entity) {
        if (!repository.existsById(entity.getId()))
            return null;
        repository.delete(entity);
        return entity;
    }
}
