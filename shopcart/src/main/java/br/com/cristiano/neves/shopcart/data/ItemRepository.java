/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cristiano.neves.shopcart.data;

import br.com.cristiano.neves.shopcart.model.Item;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author Cristiano Neves
 */
public interface ItemRepository extends MongoRepository<Item, String> {
}