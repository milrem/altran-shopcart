/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cristiano.neves.shopcart.resources;

import br.com.cristiano.neves.shopcart.model.Item;
import br.com.cristiano.neves.shopcart.service.ItemService;
import java.net.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


/**
 *
 * @author Cristiano Neves
 */
@RestController
@RequestMapping(path = "item", produces = MediaType.APPLICATION_JSON_VALUE)
public class ItemRest extends RestService<Item, String> {
    private static final long LIMIT_ON_lIST = 100;
    
    @Autowired
    public ItemRest(ItemService service) {
        super(service);
    }
    
    @Override
    protected String getId(Item entity) {
        return entity.getId();
    }
}
