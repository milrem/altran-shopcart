/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cristiano.neves.shopcart.service;

import br.com.cristiano.neves.shopcart.data.CartRepository;
import br.com.cristiano.neves.shopcart.model.Cart;
import java.util.List;
import java.util.UUID;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

/**
 *
 * @author milrem
 */
@Service
public class CartService implements IService<Cart, String> {
    private CartRepository repository;
    
    @Autowired
    public CartService(CartRepository repository) {
        this.repository = repository;
    }
    
    private String makeNewId() {
        String id = UUID.randomUUID().toString();
        if (repository.existsById(id)) {
            return makeNewId();
        } else {
            return id;
        }
    }

    @Override
    public Cart create(Cart entity) {
        Cart newEntity = new Cart();
        newEntity.setId(makeNewId());
        newEntity.setOwner(entity.getOwner());
        repository.insert(newEntity);
        return repository.findById(newEntity.getId()).get();
    }
        
    @SneakyThrows
    public Cart create(@NonNull String ownerId) {
        Cart newEntity = new Cart();
        newEntity.setOwner(ownerId);
        if (repository.findOne(Example.of(newEntity)).isPresent())
            throw new Exception("That used alread has a Cart. Each user could have only one");
        return create(newEntity);
    }
    
    @Override
    public List<Cart> list() {
        return repository.findAll();
    }
    
    @Override
    public Cart find(String id) {
        return repository.findById(id).get();
    }
    
    @Override
    public Cart update(Cart entity) {
        Cart find = new Cart();
        find.setId(entity.getId());
        if (!repository.exists(Example.of(find)))
            return null;
        return repository.save(entity);
    }
    
    @Override
    public Cart delete(Cart entity) {
        Cart find = new Cart();
        find.setId(entity.getId());
        if (!repository.exists(Example.of(find)))
            return null;
        repository.delete(entity);
        return find;
    }
}
