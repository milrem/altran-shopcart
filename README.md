# GIT clone

To clone that projects, simply open a tarminal in a blank folder and execute git clone git clone git@bitbucket.org:milrem/altran-shopcart.git 

# Shopcart

This project was create with Maven and Spring Boot.

To run the backend rest services simply open a terminal in the root folder and execute java -jar shopcart/target/shopcart-1.0.jar. The Spring Boot will load the Jetty server with rest services on `http://localhost:8080`.

# ShopcartUi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.23.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


