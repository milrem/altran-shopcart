import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../user';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css'],
})
export class CreateUserComponent implements OnInit {
  user: User = new User();
  saved = false;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
  }

  newItem(): void {
    this.saved = false;
    this.user = new User;
  }

  save() {
    this.userService.create(this.user)
      .subscribe(data => console.log(data), error => console.log(error));
    this.user = new User();
  }

  onSubmit() {
    this.saved = true;
    this.save();
  } 
}
