import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { UserListComponent } from './list/user-list.component';
import { CreateUserComponent } from './create/user-create.component';
import { UpdateUserComponent } from './update/user-update.component';
import { UserDetailComponent } from './detail/user-detail.component';


const routes: Routes = [
  {path: 'users', component: UserListComponent},
  {path: 'user/:id', component: UserDetailComponent},
  {path: 'users/add', component: CreateUserComponent},
  {path: 'user/:id/update/', component: UpdateUserComponent},
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class UserRoutingModule { }
