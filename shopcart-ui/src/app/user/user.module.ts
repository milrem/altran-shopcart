import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms'; 

import { UserListComponent } from './list/user-list.component';
import { CreateUserComponent } from './create/user-create.component';
import { UserDetailComponent } from './detail/user-detail.component';
import { UpdateUserComponent } from './update/user-update.component';
import { UserRoutingModule } from './user-routing.module';
import { UserService } from '../services/user.service';

@NgModule({
  declarations: [
    UserListComponent,
    CreateUserComponent,
    UserDetailComponent,
    UpdateUserComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    UserRoutingModule
  ],
  providers: [UserService]
})
export class UserModule { }
