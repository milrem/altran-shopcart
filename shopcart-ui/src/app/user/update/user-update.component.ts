import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UpdateUserComponent implements OnInit {
  id: string;
  user: User

  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService) { }

  ngOnInit() {
    this.user = new User();
    this.id = this.route.snapshot.params['id'];
    this.userService.getById(this.id)
      .subscribe(data => {
        console.log(data);
        this.user = data;
      },
      error => console.log(error));
  }

  updateItem() {
    this.userService.update(this.id, this.user)
      .subscribe(data => console.log(data), error => console.log(error));
    this.user = new User();
    this.navigateToList();
  }

  onSubmit() {
    this.updateItem();
  }

  navigateToList() {
    this.router.navigate(['/users']);
  }

}
