import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = `${environment.apiendpoint}/user`;

  constructor(private http: HttpClient) { }

  getById(id: string): Observable<any> {
    return this.http.get(`'${this.baseUrl}/${id}`);
  }

  create(user: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, user);
  }

  update(id: string, user: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, user);
  }

  delete(id: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
