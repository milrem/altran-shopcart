import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  private baseUrl = `${environment.apiendpoint}/item`;

  constructor(private http: HttpClient) { }

  getById(id: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  create(item: Object): Observable<Object> {
    return this.http.post(this.baseUrl, item);
  }

  update(id: string, item: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, item);
  }

  delete(id: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getList(): Observable<any> {
    return this.http.get(this.baseUrl);
  }
}
