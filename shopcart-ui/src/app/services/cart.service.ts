import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private baseUrl = `${environment.apiendpoint}/cart`;

  constructor(private http: HttpClient) { }

  getById(id: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  create(owner: string): Observable<Object> {
    return this.http.post(`${this.baseUrl}/${owner}`, '');
  }

  update(id: string, item: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, item);
  }

  delete(id: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
