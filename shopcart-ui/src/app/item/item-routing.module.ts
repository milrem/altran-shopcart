import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ItemListComponent } from './list/item-list.component';
import { CreateItemComponent } from './create/item-create.component';
import { UpdateItemComponent } from './update/item-update.component';
import { ItemDetailComponent } from './detail/item-detail.component';


const routes: Routes = [
  {path: 'items', component: ItemListComponent},
  {path: 'item/:id', component: ItemDetailComponent},
  {path: 'items/add', component: CreateItemComponent},
  {path: 'item/:id/update/', component: UpdateItemComponent},
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ItemRoutingModule { }
