import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ItemService } from '../../services/item.service';
import { Item } from '../item';

@Component({
  selector: 'app-item-create',
  templateUrl: './item-create.component.html',
  styleUrls: ['./item-create.component.css'],
})
export class CreateItemComponent implements OnInit {
  item: Item = new Item();
  saved = false;

  constructor(private itemService: ItemService, private router: Router) { }

  ngOnInit() {
  }

  newItem(): void {
    this.saved = false;
    this.item = new Item;
  }

  save() {
    this.itemService.create(this.item)
      .subscribe(data => console.log(data), error => console.log(error));
    this.item = new Item();
  }

  onSubmit() {
    this.saved = true;
    this.save();
  } 
}
