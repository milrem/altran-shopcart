import { Component, OnInit } from '@angular/core';
import { Item } from '../item';
import { ActivatedRoute, Router } from '@angular/router';
import { ItemService } from '../../services/item.service';

@Component({
  selector: 'app-item-update',
  templateUrl: './item-update.component.html',
  styleUrls: ['./item-update.component.css']
})
export class UpdateItemComponent implements OnInit {
  id: string;
  item: Item

  constructor(private route: ActivatedRoute, private router: Router, private itemService: ItemService) { }

  ngOnInit() {
    this.item = new Item();
    this.id = this.route.snapshot.params['id'];
    this.itemService.getById(this.id)
      .subscribe(data => {
        console.log(data);
        this.item = data;
      },
      error => console.log(error));
  }

  updateItem() {
    this.itemService.update(this.id, this.item)
      .subscribe(data => console.log(data), error => console.log(error));
    this.item = new Item();
    this.navigateToList();
  }

  onSubmit() {
    this.updateItem();
  }

  navigateToList() {
    this.router.navigate(['/items']);
  }

}
