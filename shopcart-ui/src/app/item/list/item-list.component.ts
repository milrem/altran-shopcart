import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ItemService } from '../../services/item.service';
import { Item } from '../item'

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css'],
})
export class ItemListComponent implements OnInit {
  items: Observable<Item[]>;

  constructor(private itemService: ItemService, private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.items = this.itemService.getList();
  }

  deleteItem(id: string) {
    this.itemService.delete(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error)
      );
  }

  itemDetails(id: string) {
    this.router.navigate(['item', id]);
  }

  creatNewItem() {
    this.router.navigate(['items/add']);
  }

}
