import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms'; 

import { ItemListComponent } from './list/item-list.component';
import { CreateItemComponent } from './create/item-create.component';
import { ItemDetailComponent } from './detail/item-detail.component';
import { UpdateItemComponent } from './update/item-update.component';
import { ItemRoutingModule } from './item-routing.module';
import { ItemService } from '../services/item.service';

@NgModule({
  declarations: [
    ItemListComponent,
    CreateItemComponent,
    ItemDetailComponent,
    UpdateItemComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ItemRoutingModule
  ],
  providers: [ItemService]
})
export class ItemModule { }
