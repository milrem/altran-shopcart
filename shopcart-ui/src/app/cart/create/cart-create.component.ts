import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../../services/cart.service';
import { Cart } from '../cart';

@Component({
  selector: 'app-cart-create',
  templateUrl: './cart-create.component.html',
  styleUrls: ['./cart-create.component.css'],
})
export class CreateCartComponent implements OnInit {
  cart: Cart = new Cart();
  saved = false;

  constructor(private cartService: CartService, private router: Router) { }

  ngOnInit() {
  }

  newItem(): void {
    this.saved = false;
    this.cart = new Cart;
  }

  save() {
    this.cartService.create(this.cart.owner)
      .subscribe(data => console.log(data), error => console.log(error));
    this.cart = new Cart();
  }

  onSubmit() {
    this.saved = true;
    this.save();
  } 
}
