import { Component, OnInit } from '@angular/core';
import { Cart } from '../cart';
import { ActivatedRoute, Router } from '@angular/router';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'app-cart-update',
  templateUrl: './cart-update.component.html',
  styleUrls: ['./cart-update.component.css']
})
export class UpdateCartComponent implements OnInit {
  id: string;
  cart: Cart

  constructor(private route: ActivatedRoute, private router: Router, private cartService: CartService) { }

  ngOnInit() {
    this.cart = new Cart();
    this.id = this.route.snapshot.params['id'];
    this.cartService.getById(this.id)
      .subscribe(data => {
        console.log(data);
        this.cart = data;
      },
      error => console.log(error));
  }

  updateItem() {
    this.cartService.update(this.id, this.cart)
      .subscribe(data => console.log(data), error => console.log(error));
    this.cart = new Cart();
    this.navigateToList();
  }

  onSubmit() {
    this.updateItem();
  }

  navigateToList() {
    this.router.navigate(['/carts']);
  }

}
