import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { CartService } from '../../services/cart.service';
import { Cart } from '../cart'

@Component({
  selector: 'app-cart-list',
  templateUrl: './cart-list.component.html',
  styleUrls: ['./cart-list.component.css'],
})
export class CartListComponent implements OnInit {
  carts: Observable<Cart[]>;

  constructor(private cartService: CartService, private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.carts = this.cartService.getList();
  }

  deleteItem(id: string) {
    this.cartService.delete(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error)
      );
  }

  itemDetails(id: string) {
    this.router.navigate(['cart', id]);
  }

  creatNewItem() {
    this.router.navigate(['carts/add']);
  }

}
