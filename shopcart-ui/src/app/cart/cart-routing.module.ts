import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CartListComponent } from './list/cart-list.component';
import { CreateCartComponent } from './create/cart-create.component';
import { UpdateCartComponent } from './update/cart-update.component';
import { CartDetailComponent } from './detail/cart-detail.component';


const routes: Routes = [
  {path: 'carts', component: CartListComponent},
  {path: 'cart/:id', component: CartDetailComponent},
  {path: 'carts/add', component: CreateCartComponent},
  {path: 'cart/:id/update/', component: UpdateCartComponent},
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class CartRoutingModule { }
