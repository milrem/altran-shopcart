import { Component, OnInit } from '@angular/core';
import { Cart } from '../cart';
import { ActivatedRoute, Router } from '@angular/router';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'app-cart-detail',
  templateUrl: './cart-detail.component.html',
  styleUrls: ['./cart-detail.component.css']
})
export class CartDetailComponent implements OnInit {
  id: string;
  cart: Cart;

  constructor(private route: ActivatedRoute, private router: Router, private cartService: CartService) { }

  ngOnInit() {
    this.cart = new Cart();
    this.id = this.route.snapshot.params['id'];
    this.cartService.getById(this.id)
      .subscribe(data => {
        console.log(data);
        this.cart = data;
      },
      error => console.log(error));
  }

  navigateToList() {
    this.router.navigate(['/carts']);
  }

}
