import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms'; 

import { CartListComponent } from './list/cart-list.component';
import { CreateCartComponent } from './create/cart-create.component';
import { CartDetailComponent } from './detail/cart-detail.component';
import { UpdateCartComponent } from './update/cart-update.component';
import { CartRoutingModule } from './cart-routing.module';
import { CartService } from '../services/cart.service';

@NgModule({
  declarations: [
    CartListComponent,
    CreateCartComponent,
    CartDetailComponent,
    UpdateCartComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    CartRoutingModule
  ],
  providers: [CartService]
})
export class CartModule { }
